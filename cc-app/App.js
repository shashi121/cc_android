import React, { Component } from 'react';
import {
  StyleSheet,
  KeyboardAvoidingView,
  View,
  StatusBar 
} from 'react-native';


import Routes from './src/Routes';

export default class App extends Component<{}> {
  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <StatusBar
           backgroundColor="#1c313a"
           barStyle="light-content"
         />
        
        <Routes/>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    flex: 1,
  }
});