import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity ,
  KeyboardAvoidingView ,
  Alert,
} from 'react-native';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';

export default class Logo extends Component<{}> {
  signup() {
    Actions.signup()
  }
  constructor(props) {

   super(props)

   this.state = {

     nama: '',
    
     jurusan: '',
     semester: '',
     data:null

   }

 } 
 

  
 InsertStudentRecordsToServer = () =>{

    axios.post('http://70bb2ddc.ngrok.io/vehicle/make', {
    nama : this.state.nama

  })
        .then(function (response) {
           Alert.alert('Credentials', `${this.state.nama}`);
        })
        .catch(function (error) {
          if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              Alert.alert('Credentials', `${this.state.nama}`);
              console.log(error.response.data);
            } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(error.request);
            } else {
              // Something happened in setting up the request that triggered an Error
              Alert.alert('Credentials', `${this.state.nama}`);
              console.log('Error', error.message);
            }
  });


     

}


	render(){
		return(
     
			<KeyboardAvoidingView  style={styles.container} behaviour="padding" enabled>
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Email"
              placeholderTextColor = "#ffffff"
              selectionColor="#fff"
              keyboardType="email-address"
              onChangeText={ TextInputValue => this.setState({ nama : TextInputValue }) }
              onSubmitEditing={()=> this.password.focus()}
              />
          <TextInput style={styles.inputBox} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor = "#ffffff"
              selectionColor="#fff"
              onChangeText={ TextInputValue => this.setState({ jurusan : TextInputValue }) }
              ref={(input) => this.password = input}
              />  
              <TouchableOpacity onPress={this.signup}><Text style={styles.signupButton}> Forgot Password?</Text></TouchableOpacity>
      
           <TouchableOpacity style={styles.button} onPress={this.InsertStudentRecordsToServer}>
             <Text style={styles.buttonText}>{this.props.type}</Text>
           </TouchableOpacity>  
             
  		</KeyboardAvoidingView>
			)
	}
}

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center'
  },

  inputBox: {
    width:300,
    backgroundColor:'red',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'#000000',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'red',
     borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }
  
});